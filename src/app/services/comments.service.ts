import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';

import { Comment } from '../comments';

@Injectable({
  providedIn: 'root'
})
export class CommentsService {
  comments$: Observable<Comment[]>;

  constructor(private firestore: AngularFirestore) {}

  /* Add Comment */
  AddComment(Comment: Comment) {
  return new Promise<any>((resolve, reject) => {
  this.firestore.collection("comments").add(Comment)
  .then(res => {
  resolve(res);
  }, err => reject(err));
  });
  }

  /* Get Comments list */
  getComments() {
  return this.firestore.collection("comments").valueChanges();
  }

}
