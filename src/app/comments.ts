import { ModuleWithComponentFactories } from "@angular/core";

export interface Comment{
  content? : string;
  date? : Date | any;
}
