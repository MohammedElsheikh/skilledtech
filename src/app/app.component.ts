import { CommentsService } from './services/comments.service';
import { Component, OnInit } from '@angular/core';
import { Comment } from './comments';
import * as moment from 'moment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'skilledtech-test';
  Comments:Comment[] = []
  moment = moment;
  limit = 5;


  comment:string
  constructor(private commentsService:CommentsService){

  }

  ngOnInit(){
    this.getComments();
  }


  addComment(){
    this.commentsService.AddComment({content:this.comment, date: new Date()});
    this.comment = ''
  }



  getComments(){
    this.commentsService.getComments().subscribe((res)=>{
      this.Comments = res;
      this.Comments = this.Comments.sort((b,a) => (a.date.seconds > b.date.seconds) ? 1 : ((b.date.seconds > a.date.seconds) ? -1 : 0)) // sort by time
    });


  }


  changeLimit(){
      if(this.limit < this.Comments.length){
        this.limit += 5;
      }
  }
}
